package it.uniroma3;


import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.uniroma3.model.Cane;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.model.Padrone;
import it.uniroma3.service.CaneService;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.CorsoService;
import it.uniroma3.service.PadroneService;

@SpringBootApplication
public class SiwInuNoKunrenSpringApplication {

	@Autowired
	private PadroneService padroneService;
	
	@Autowired
	private CaneService caneService;
	
	@Autowired
	private CorsoService corsoService;
	
	@Autowired
	private CentroService centroService;
	
	public static void main(String[] args) {
		SpringApplication.run(SiwInuNoKunrenSpringApplication.class, args);
	}
	
	@PostConstruct
	public void init() {
		Corso corso1 = new Corso("Cani informativi sul web","Lunedì","6.00","Mr. Paolo Merialdo");
		Corso corso2 = new Corso("Analisi e progettazione del cane","Martedì","17.30","Coach Luca Cabibbo");
		Corso corso3 = new Corso("Cani di calcolatori","Sabato","15.00","Maestro Giuseppe di Battista");
		Centro centro = new Centro ("SoRA Center","Via Grele 7","sora@alliance.it","0871087100",90);
		
		corsoService.save(corso1);
		corsoService.save(corso2);
		corsoService.save(corso3);
		centroService.save(centro);
		
		centro.aggiungiCorso(corso1);
		centro.aggiungiCorso(corso2);
		centro.aggiungiCorso(corso3);
		
		corsoService.save(corso1);
		corsoService.save(corso2);
		corsoService.save(corso3);
		centroService.save(centro);

	}
}
