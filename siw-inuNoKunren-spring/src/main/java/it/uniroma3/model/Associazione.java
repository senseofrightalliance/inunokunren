package it.uniroma3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Associazione {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String nome;
	
	@OneToOne
	private Responsabile responsabile;
	
	@OneToMany
	@JoinColumn(name="associazione_id")
	private List<Centro> centri;
	
	@OneToMany
	@JoinColumn(name="associazione_id")
	private List<Padrone> padroni;
	
	public Associazione() {}

	public Associazione(String nome,Responsabile responsabile) {
		super();
		this.nome = nome;
		this.responsabile = responsabile;
		this.centri = new ArrayList<Centro>();
		this.padroni = new ArrayList<Padrone>();
	}
	
	public void assegnaCentro(Centro c) {
		centri.add(c);
	}

	public void assegnaPadrone(Padrone p) {
		this.padroni.add(p);
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Responsabile getResponsabile() {
		return responsabile;
	}

	public void assegnaResponsabile(Responsabile responsabile) {
		this.responsabile = responsabile;
	}

	public List<Centro> getCentri() {
		return centri;
	}

	public void setCentri(List<Centro> centri) {
		this.centri = centri;
	}

	public List<Padrone> getPadroni() {
		return padroni;
	}

	public void setPadroni(List<Padrone> padroni) {
		this.padroni = padroni;
	}
}
