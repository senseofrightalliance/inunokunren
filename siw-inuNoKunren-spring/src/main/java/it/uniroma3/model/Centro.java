package it.uniroma3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Centro {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String nome;
	private String indirizzo;
	private String mail;
	private String telefono;
	private int capienza;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="centroDiAddestramento_id")
	private List<Corso> corsi;
	
	@OneToOne
	private Responsabile responsabile;
	
	public Centro() {
		this.corsi=new ArrayList<Corso>();
	}

	public Centro(String nome, String indirizzo, String mail, String telefono, int capienza) {
		super();
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.mail = mail;
		this.telefono = telefono;
		this.capienza = capienza;
		this.corsi=new ArrayList<Corso>();
	}
	
	public void assegnaResponsabile(Responsabile responsabile) {
		this.responsabile = responsabile;
	}
	
	public void aggiungiCorso(Corso c) {
		this.corsi.add(c);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getCapienza() {
		return capienza;
	}

	public void setCapienza(int capienza) {
		this.capienza = capienza;
	}

	public List<Corso> getCorsi() {
		return corsi;
	}

	public void setCorsi(List<Corso> corsi) {
		this.corsi = corsi;
	}

	public Responsabile getResponsabile() {
		return responsabile;
	}
}
