package it.uniroma3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Corso {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String nome;
	private String giorno;
	private String ora;
	private String addestratore;
	
	@ManyToMany(mappedBy="corsi",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Cane> cani;
	
	public Corso() {
		this.cani = new ArrayList<Cane>();
	}

	public Corso(String nome, String giorno, String ora, String addestratore) {
		super();
		this.nome = nome;
		this.giorno = giorno;
		this.ora = ora;
		this.addestratore = addestratore;
		this.cani = new ArrayList<Cane>();
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getData() {
		return giorno;
	}

	public void setData(String data) {
		this.giorno = data;
	}

	public String getOra() {
		return ora;
	}

	public void setOra(String ora) {
		this.ora = ora;
	}

	public String getAddestratore() {
		return addestratore;
	}

	public void setAddestratore(String descrizione) {
		this.addestratore = descrizione;
	}

	public List<Cane> getCani() {
		return cani;
	}

	public void setCani(List<Cane> cani) {
		this.cani = cani;
	}
}
