package it.uniroma3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Padrone {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String nome;
	
	@Column(nullable=false)
	private String cognome;
	
	@Column(nullable=false)
	private String mail;
	
	@Column(nullable=false)
	private String telefono;
	
	@Column(nullable=false)
	private String dataDiNascita;
	
	@Column(nullable=false)
	private String city;
	
	@Column
	private String utente;
	
	@OneToMany
	@JoinColumn(name="padrone_id")
	private List<Cane> cani;
	
	public Padrone() {
		this.cani = new ArrayList<Cane>();
	}

	public Padrone(String nome, String cognome, String mail, String telefono, String dataDiNascita, String city, String utente) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.mail = mail;
		this.telefono = telefono;
		this.dataDiNascita = dataDiNascita;
		this.city = city;
		this.utente=utente;
		this.cani = new ArrayList<Cane>();
	}
	
	public void assegnaPadrone() {
		for(Cane c : this.cani) {
			c.setPadrone(this);
		}
	}
	
	public void iscriviCane(Cane cane,Corso corso) {
		corso.getCani().add(cane);
		cane.getCorsi().add(corso);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDataDiNascita() {
		return dataDiNascita;
	}

	public void setDataDiNascita(String dataDiNascita) {
		this.dataDiNascita = dataDiNascita;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public List<Cane> getCani() {
		return this.cani;
	}
	
	public void setCani(List<Cane> cani) {
		this.cani=cani;
	}

	public String getUtente() {
		return utente;
	}

	public void setUtente(String utente) {
		this.utente = utente;
	}
}