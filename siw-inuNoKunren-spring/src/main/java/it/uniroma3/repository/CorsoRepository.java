package it.uniroma3.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Corso;

public interface CorsoRepository extends CrudRepository<Corso, Long> {


	public List<Corso> findByNome(String nome);
	
	public List<Corso> findAll();

}