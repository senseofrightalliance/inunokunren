package it.uniroma3.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Cane;

public interface CaneRepository extends CrudRepository<Cane, Long> {


	public List<Cane> findByNome(String nome);
	
	public List<Cane> findAll();

}
