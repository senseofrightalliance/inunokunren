package it.uniroma3.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Padrone;

public interface PadroneRepository extends CrudRepository<Padrone, Long> {

	public List<Padrone> findByCity(String city);

	public List<Padrone> findByNomeAndCognomeAndCity(String nome, String cognome, String city);

	public List<Padrone> findByUtente(String utente);
	
	
}
