package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Corso;
import it.uniroma3.repository.CorsoRepository;

@Transactional
@Service
public class CorsoService {
	
	@Autowired
	private CorsoRepository corsoRepository; 
	
	public Corso save(Corso corso) {
		return this.corsoRepository.save(corso);
	}

	public List<Corso> findAll() {
		return (List<Corso>) this.corsoRepository.findAll();
	}
	
	public Corso findById(Long id) {
		Optional<Corso> corso = this.corsoRepository.findById(id);
		if (corso.isPresent()) 
			return corso.get();
		else
			return null;
	}

	/**/
}