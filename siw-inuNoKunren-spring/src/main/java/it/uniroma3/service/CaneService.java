package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Cane;
import it.uniroma3.repository.CaneRepository;

@Transactional
@Service
public class CaneService {
	
	@Autowired
	private CaneRepository caneRepository; 
	
	public Cane save(Cane cane) {
		return this.caneRepository.save(cane);
	}

	public List<Cane> findByNome(String nome) {
		return this.caneRepository.findByNome(nome);
	}

	public List<Cane> findAll() {
		return (List<Cane>) this.caneRepository.findAll();
	}
	
	public Cane findById(Long id) {
		Optional<Cane> cane = this.caneRepository.findById(id);
		if (cane.isPresent()) 
			return cane.get();
		else
			return null;
	}

	public boolean alreadyExists(Cane cane) {
		List<Cane> cani = this.caneRepository.findByNome(cane.getNome());
		if (cani.size() > 0)
			return true;
		else 
			return false;
	}	
}
