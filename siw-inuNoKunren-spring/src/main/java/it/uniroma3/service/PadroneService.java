package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Padrone;
import it.uniroma3.repository.PadroneRepository;

@Transactional
@Service
public class PadroneService {
	
	@Autowired
	private PadroneRepository padroneRepository; 
	
	public Padrone save(Padrone padrone) {
		return this.padroneRepository.save(padrone);
	}

	public List<Padrone> findByCity(String city) {
		return this.padroneRepository.findByCity(city);
	}

	public List<Padrone> findAll() {
		return (List<Padrone>) this.padroneRepository.findAll();
	}
	
	public Padrone findById(Long id) {
		Optional<Padrone> padrone = this.padroneRepository.findById(id);
		if (padrone.isPresent()) 
			return padrone.get();
		else
			return null;
	}
	
	public List<Padrone> findByUtente(String utente) {
		return this.padroneRepository.findByUtente(utente);
	}
	

	public boolean alreadyExists(Padrone padrone) {
		List<Padrone> padroni = this.padroneRepository.findByUtente(padrone.getUtente());
		if (padroni.size() > 0)
			return true;
		else 
			return false;
	}	
}