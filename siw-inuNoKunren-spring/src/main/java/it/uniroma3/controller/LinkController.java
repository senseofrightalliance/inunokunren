package it.uniroma3.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LinkController {

	
	 @RequestMapping("/getHome")
	    public String getHome(Model model) {
	        return "index";
	    }
	 
	 @RequestMapping("/getCentri")
	    public String getCentri(Model model) {
	        return "centri";
	    }
	 
	 @RequestMapping("/getAbout")
	    public String getAbout(Model model) {
	        return "about";
	    }
	 
	 @RequestMapping("/getContatti")
	    public String getContatti(Model model) {
	        return "contatti";
	    }
	 
	 @RequestMapping("/getAreaRiservata")
	    public String getAreaRiservata(Model model) {
	        return "userpassword";
	    }









}
