package it.uniroma3.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.controller.validator.PadroneValidator;
import it.uniroma3.model.Padrone;
import it.uniroma3.service.PadroneService;

@Controller
public class PadroneController {

	@Autowired
	private PadroneService padroneService;

	@Autowired
	private PadroneValidator validator;

	@RequestMapping("/padroni")
	public String padroni(Model model) {
		model.addAttribute("padroni", this.padroneService.findAll());
		return "padroniList";
	}

	@RequestMapping("/addPadrone")
	public String addPadrone(Model model) {
		model.addAttribute("padrone", new Padrone());
		return "padroneForm";
	}

	@RequestMapping(value = "/padrone/{id}", method = RequestMethod.GET)
	public String getPadrone(@PathVariable("id") Long id, Model model) {
		model.addAttribute("padrone", this.padroneService.findById(id));
		return "showPadrone";
	}

	@RequestMapping(value="/showPadrone", method=RequestMethod.GET)
	public String getLogPadrone(@PathVariable("utente") String utente, Model model) {	
		model.addAttribute("padrone", this.padroneService.findByUtente(utente).get(0));
		return "showPadrone";
	}
	
	
	@RequestMapping(value = "/loginPadrone", method = RequestMethod.POST)
	public String loginPadrone(@Valid @ModelAttribute("padrone") Padrone padrone, Model model) {
		if (this.padroneService.alreadyExists(padrone)) {
			/*model.addAttribute("exists", "Padrone already exists");
			return "padroneForm";*/
			/*model.addAttribute("padroni", this.padroneService.findAll());*/
			/*model.addAttribute("padrone", this.padroneService.findByUtente(padrone.getUtente()));
			padroneService.save(padrone);*/
			
			return this.getLogPadrone(padrone.getUtente(),model);
		} else {
			if (!this.padroneService.alreadyExists(padrone)) {
				
				return "user&password";
			}
		}
		return "user&password";
	}

	@RequestMapping(value = "/padrone", method = RequestMethod.POST)
	public String newPadrone(@Valid @ModelAttribute("padrone") Padrone padrone, Model model,
			BindingResult bindingResult) {
		this.validator.validate(padrone, bindingResult);

		if (this.padroneService.alreadyExists(padrone)) {
			model.addAttribute("exists", "Padrone already exists");
			return "padroneForm";
		} else {
			if (!bindingResult.hasErrors()) {
				this.padroneService.save(padrone);
				model.addAttribute("padroni", this.padroneService.findAll());
				return "user&password";
			}
		}
		return "padroneForm";
	}

}
