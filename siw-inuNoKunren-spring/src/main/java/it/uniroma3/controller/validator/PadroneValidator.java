package it.uniroma3.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Padrone;

@Component
public class PadroneValidator implements Validator {

	   @Override
	    public void validate(Object o, Errors errors) {
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cognome", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mail", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dataDiNascita", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "utente", "required");
	     
	        

	    }

	    @Override
	    public boolean supports(Class<?> aClass) {
	        return Padrone.class.equals(aClass);
	    }	
}
