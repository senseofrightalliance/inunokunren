package it.uniroma3.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Cane;

@Component
public class CaneValidator implements Validator {

	   @Override
	    public void validate(Object o, Errors errors) {
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "eta", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "razza", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "livello", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sesso", "required");
	    }

	    @Override
	    public boolean supports(Class<?> aClass) {
	        return Cane.class.equals(aClass);
	    }	
}
