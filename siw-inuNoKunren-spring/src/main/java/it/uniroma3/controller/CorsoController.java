package it.uniroma3.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.CorsoService;

@Controller
public class CorsoController {

	@Autowired
	private CentroService centroService;

	@Autowired
	private CorsoService corsoService;

	@RequestMapping("/centro/{idce}/corsi")
	public String corsi(@PathVariable("idce") Long idce, Model model) {
		Centro centro = centroService.findById(idce);
		model.addAttribute("centro", centro);
		model.addAttribute("corsi", centro.getCorsi());
		return "corsiList";
	}
	
	/*
	@RequestMapping(value="/corso/{id}")
	public String showCorso(Centro centro, @PathVariable("id") Long id, Model model) {
		model.addAttribute("corsi", this.centroService.findById(id).getCorsi());
		return "corsiList";
	}
	*/
	
	@RequestMapping(value = "/corso/{id}", method = RequestMethod.GET)
	public String getCorso(@PathVariable("id") Long id, Model model) {
		model.addAttribute("corso", this.corsoService.findById(id));
		return "showCorso";
	}
	
    @RequestMapping("/centro/{idce}/addCorso")
    public String addCorso(@PathVariable("idce") Long idce, Model model) {
    	Centro centro = centroService.findById(idce);
    	model.addAttribute("centro",centro);
    	model.addAttribute("corso",centro.getCorsi());
    	return "corsiList";	
    } 

	/*@RequestMapping(value = "/corso", method = RequestMethod.POST)
	public String newCorso(@Valid @ModelAttribute("corso") Corso corso, Model model, BindingResult bindingResult) {
		this.corsoService.save(corso);
		model.addAttribute("corsi", this.corsoService.findAll());
		return "corsiList";
	}*/
}
