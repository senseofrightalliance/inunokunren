package it.uniroma3.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.controller.validator.CaneValidator;
import it.uniroma3.model.Cane;
import it.uniroma3.model.Corso;
import it.uniroma3.model.Padrone;
import it.uniroma3.service.CaneService;
import it.uniroma3.service.CorsoService;
import it.uniroma3.service.PadroneService;

@Controller
public class CaneController {
	
	@Autowired
    private CaneService caneService;

    @Autowired
    private CaneValidator validator;
    
    @Autowired
    private CorsoService corsoService;
    
    @Autowired PadroneService padroneService;
    
    /*@RequestMapping("/corso/{idco}/cani")
    public String cani( @PathVariable("idco") Long idco, Model model) {
    	Corso corso = corsoService.findById(idco);
    	model.addAttribute("corso",corso);
    	model.addAttribute("cani",corso.getCani());
    	return "caniList";
    }*/
    
    @RequestMapping("/corso/{idco}/addCane")
    public String addCane(@PathVariable("idco") Long idco, Model model) {
    	Corso corso = corsoService.findById(idco);
    	model.addAttribute("corso",corso);
    	model.addAttribute("cane",new Cane());
    	corsoService.save(corso);
    	return "caneForm";	
    }
    
    @RequestMapping("/padrone/{id}/aggiungiCane")
    public String aggiungiCane(@PathVariable("id") Long id, Model model) {
    	Padrone padrone = padroneService.findById(id);
    	model.addAttribute("padrone",padrone);
    	model.addAttribute("cane",new Cane());
    	padroneService.save(padrone);
    	return "caneForm";	
    }    
    
    @RequestMapping("/padrone/{id}/cani")
    public String cani(Model model) {
        model.addAttribute("cani", this.caneService.findAll());
        return "caniList";
    }

    @RequestMapping("/addCane")
    public String addCane(Model model) {
        model.addAttribute("cane", new Cane()); 
        return "caneForm";
    }

    @RequestMapping(value = "/cane/{id}", method = RequestMethod.GET)
    public String getCane(@PathVariable("id") Long id, Model model) {
        model.addAttribute("cane", this.caneService.findById(id));
    	return "showCane";
    }

    @RequestMapping(value = "/padrone/{id}/cane", method = RequestMethod.POST)
    public String newCane(@Valid @ModelAttribute("cane") Cane cane, @PathVariable("id") Long id, Model model, BindingResult bindingResult) {
        this.validator.validate(cane, bindingResult);
        Padrone padrone = padroneService.findById(id);
        model.addAttribute("padrone",padrone);
        
        if (this.caneService.alreadyExists(cane)) {
            model.addAttribute("exists", "Cane already exists");
            return "caneForm";
        }
        else {
            if (!bindingResult.hasErrors()) {
                this.caneService.save(cane);
                padrone.getCani().add(cane);
                padroneService.save(padrone);
                model.addAttribute("cani", this.caneService.findAll());
                return "caniList";
            }
        }
        return "caneForm";
    }
}
